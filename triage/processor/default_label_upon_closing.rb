# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class DefaultLabelUponClosing < Processor
    react_to 'issue.close', 'merge_request.close', 'merge_request.merge'

    FEATURE_TYPE_LABELS = %w[direction].freeze
    MAINTENANCE_TYPE_LABELS = %w[security documentation].freeze

    def applicable?
      event.from_gitlab_org? &&
        !event.type_label_set? &&
        (has_label?(MAINTENANCE_TYPE_LABELS) ||
        has_label?(FEATURE_TYPE_LABELS))
    end

    def process
      default_label_upon_closing
    end

    def documentation
      <<~TEXT
        This processor adds default labels upon resource closing/merging.
      TEXT
    end

    private

    # As soon as we set any `~type::` label, this processor should stop doing anything else (see the `applicable?` definition)
    def default_label_upon_closing
      if has_label?(FEATURE_TYPE_LABELS)
        add_comment('/label ~"type::feature"', append_source_link: false)
      else
        add_comment('/label ~"type::maintenance"', append_source_link: false)
      end
    end

    def has_label?(labels)
      (event.label_names & labels).any?
    end
  end
end
